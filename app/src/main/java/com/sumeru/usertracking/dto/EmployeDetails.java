package com.sumeru.usertracking.dto;

import java.util.List;

/**
 * Created by Nihar on 8/20/2017.
 */
public class EmployeDetails {

    private List<EmpDTO> employeeList;

    public List<EmpDTO> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeList(List<EmpDTO> employeeList) {
        this.employeeList = employeeList;
    }
}
