package com.sumeru.usertracking.dto;

/**
 * Created by Nihar on 8/10/2017.
 */
public class UserDTO {

    private String userName;
    private String password;
    private String device_id;

    @Override
    public String toString() {
        return "UserDTO{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", device_id='" + device_id + '\'' +
                '}';
    }
    public String getDevice_id() { return device_id; }
    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
