package com.sumeru.usertracking.dto;

/**
 * Created by Nitesh on 8/20/2017.
 */
public class EmpDTO {

    private int id;
    private String name;
    private String address;


    @Override
    public String toString() {
        return "EmpDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
