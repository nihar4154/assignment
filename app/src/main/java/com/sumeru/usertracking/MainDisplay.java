package com.sumeru.usertracking;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableRow.LayoutParams;
import android.widget.Toast;

import com.sumeru.usertracking.dto.EmpDTO;
import com.sumeru.usertracking.dto.EmployeDetails;
import com.sumeru.usertracking.fingerprint.FingerPrint;

import java.util.Locale;

import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Result;

/**
 * Created by Nihar on 8/10/2017.
 */
public class MainDisplay extends Activity implements View.OnClickListener, TextToSpeech.OnInitListener, AIListener {

    String userName,userAddress;
    DummyDBHelper dummyHelper = new DummyDBHelper(this);
    EmployeDetails employeDetails = null;

    private AIService aiService;

    private TextToSpeech myTTS;
    private int MY_DATA_CHECK_CODE = 0;


   /* String companies[] = {"Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung"};
    String os[]       =  {"Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada"};
*/


    TableLayout tl;
    TableRow tr;
    TextView nameTV, addressTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            //Display home screen
            setContentView(R.layout.main_display);

        //check for TTS data
        Intent checkTTSIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

        //AI system
        final AIConfiguration config = new AIConfiguration("f7e9ca9f42f84e08a8fd85690f4d2b88",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
        aiService = AIService.getService(this, config);
        aiService.setListener(this);

            String userName = getIntent().getStringExtra("username");
            TextView tv = (TextView) findViewById(R.id.TFName);
            tv.setText(userName);
        tl = (TableLayout) findViewById(R.id.maintable);
        addHeaders();
        addData();
    }

    public void addHeaders(){

        employeDetails = dummyHelper.fetchUsersFromDB();
        /** Create a TableRow dynamically **/
        tr = new TableRow(this);
     //   tr.setBackgroundColor(Color.parseColor("#FFFFFF"));
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating a TextView to add to the row **/
      /*  TextView idTV = new TextView(this);
        idTV.setText("ID");
        idTV.setTextColor(Color.GRAY);
        idTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        idTV.setPadding(5, 5, 5, 0);
        tr.addView(idTV);  // Adding textView to tablerow.*/

        /** Creating another textview **/
        TextView nameTV = new TextView(this);
        nameTV.setText("NAME");
        nameTV.setTextSize(20);
        nameTV.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        nameTV.setTextColor(Color.BLACK);
       // nameTV.setTextColor(Color.GRAY);
        nameTV.setPadding(5, 5, 5, 0);
        nameTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(nameTV); // Adding textView to tablerow.

        /** Creating another textview **/
        TextView addressTV = new TextView(this);
        addressTV.setText("ADDRESS");
        addressTV.setTextSize(20);
        addressTV.setBackground(getResources().getDrawable(R.drawable.cell_shape));
        addressTV.setTextColor(Color.BLACK);
      //  addressTV.setTextColor(Color.GRAY);
        addressTV.setPadding(5, 5, 5, 0);
        addressTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(addressTV); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        // we are adding two textviews for the divider because we have two columns
        tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating another textview **/
       /* TextView divider = new TextView(this);
        divider.setText("------");
        divider.setTextColor(Color.GREEN);
        divider.setPadding(5, 0, 0, 0);
        divider.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider); // Adding textView to tablerow.*/

        TextView divider2 = new TextView(this);
      //  divider2.setText("----------");
        divider2.setTextColor(Color.BLACK);
        divider2.setPadding(5, 0, 0, 0);
        divider2.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider2); // Adding textView to tablerow.

        TextView divider3 = new TextView(this);
       // divider3.setText("-----------");
        divider3.setTextColor(Color.BLACK);
        divider3.setPadding(5, 0, 0, 0);
        divider3.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider3); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
    }

    /** This function add the data to the table **/
    public void addData(){

        for (int i = 0; i < employeDetails.getEmployeeList().size(); i++)
        {
            EmpDTO data = employeDetails.getEmployeeList().get(i);
            /** Create a TableRow dynamically **/
            final TableRow tr = new TableRow(this);
            tr.setLayoutParams(new LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));
            tr.setClickable(true);

            /** Creating a TextView to add to the row **/
       /*     idTV = new TextView(this);
            idTV.setText(companies[i]);
            idTV.setTextColor(Color.RED);
            idTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            idTV.setPadding(5, 5, 5, 5);
            tr.setId(i);
            tr.addView(idTV);  // Adding textView to tablerow.*/

            /** Creating another textview **/
            nameTV = new TextView(this);
            nameTV.setText(data.getName());
            nameTV.setTextSize(20);
            nameTV.setTextColor(Color.BLACK);
            nameTV.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            nameTV.setPadding(5, 5, 5, 5);
            nameTV.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            tr.setId(i);
            tr.addView(nameTV); // Adding textView to tablerow.

            /** Creating another textview **/
            addressTV = new TextView(this);
            addressTV.setText(data.getAddress());
            addressTV.setTextSize(20);
            addressTV.setTextColor(Color.BLACK);
            addressTV.setBackground(getResources().getDrawable(R.drawable.cell_shape));
            addressTV.setPadding(5, 5, 5, 5);
            addressTV.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            tr.setId(i);
            tr.addView(addressTV); // Adding textView to tablerow.

            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tt = (TextView) tr.getChildAt(0);
                    String data = tt.getText().toString();
                    userName = data;

                    TextView ttv = (TextView) tr.getChildAt(1);
                    String address = ttv.getText().toString();
                    userAddress = address;
                    Toast.makeText(MainDisplay.this, "Row Clicked: "+data,Toast.LENGTH_SHORT).show();

                    //implement Text to speech
                    speakWords("Do you want to navigate "+data+"'s place?");

                 /*   try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    //TODO
                    while(myTTS.isSpeaking()){
                        //wait for finish
                    }
                    aiService.startListening();

                }
            });

            // Add the TableRow to the TableLayout
            tl.addView(tr, new TableLayout.LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));
        }
    }

    private void speakWords(String speech) {

        //speak straight away
        myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null, null);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //the user has the necessary data - create the TTS
                myTTS = new TextToSpeech(this, this);
            }
            else {
                //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }


    //setup TTS
    public void onInit(int initStatus) {

        //check for successful instantiation
        if (initStatus == TextToSpeech.SUCCESS) {
            if(myTTS.isLanguageAvailable(Locale.UK)==TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.UK);
        }
        else if (initStatus == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }



    //AI System
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResult(AIResponse response) {
        Result result = response.getResult();
        String res = result.getResolvedQuery();
        // Get parameters
        if (result.getParameters() != null && !result.getParameters().isEmpty()) {
              System.out.println("Text &&&&&&&&&&&&&&&&&&&&&&&&&&&&: "+ res);
        } else
            System.out.println("Text &&&&&&&&&&&&&&&&&&&&&&&&&&&&2: "+ res);

        prepareResponse(res);
    }

    private void prepareResponse(String res){
        if(res == null || res.isEmpty()){
            Toast.makeText(this, "Please say 'YES' or 'NO'", Toast.LENGTH_SHORT).show();
        } else if((res.equalsIgnoreCase("yes"))){
            //navigate map
            Toast.makeText(this, "Navigating "+userName+"'s place!", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(MainDisplay.this, MapActivity.class);
            i.putExtra("destLoc", userAddress);
            startActivity(i);

            userName = null;
        } else if(res.equalsIgnoreCase("no")){
            // do nothing
            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Please say 'YES' or 'NO'", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(AIError error) {
        System.out.println("ERROR &&&&&&&&&&&&&&&&&&&&&&&&&&&&: "+error.toString());
        Toast.makeText(this, "voice not recognized!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }
}
