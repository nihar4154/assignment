package com.sumeru.usertracking;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sumeru.usertracking.dto.UserDTO;

/**
 * Created by Nihar on 8/10/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    SQLiteDatabase db;
    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME ="main_users.db";
    private static final String TABLE_NAME ="main_users";
    private static final String COLUMN_ID ="id";
    private static final String COLUMN_UNAME ="username";
    private static final String COLUMN_PASS ="password";
    private static final String COLUMN_DEVICE_ID ="device_id";

    private static final String TABLE_CREATE ="CREATE TABLE main_users(id int primary key not null, "+
            " username text not null, password text not null, device_id text)" ;

    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        this.db =db;
    }

    public boolean isDeviceRegistered(UserDTO userDTO){
        db = this.getReadableDatabase();
        String[] columns ={"username"};
        Cursor c = db.query(TABLE_NAME, columns, "device_id=?", new String[] { userDTO.getDevice_id() }, null, null, null);
       // String query = "select username from "+TABLE_NAME+ " where device_id = "+userDTO.getDevice_id();
      //  Cursor c = db.rawQuery(query, null);
        if(c.moveToFirst()){
          return true;
        }
        return false;
    }
    public boolean isDuplicateUserName(UserDTO userDTO){
        db = this.getReadableDatabase();
        String[] columns ={"username"};
        Cursor c = db.query(TABLE_NAME, columns, "username=?", new String[] { userDTO.getUserName() }, null, null, null);
        // String query = "select username from "+TABLE_NAME+ " where device_id = "+userDTO.getDevice_id();
        //  Cursor c = db.rawQuery(query, null);
        if(c.moveToFirst()){
            return true;
        }
        return false;
    }

    public void insertUser(UserDTO userDTO){

        System.out.println(userDTO.toString());
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String query = "select * from "+TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        int count = c.getCount();

        values.put(COLUMN_ID, count);
        values.put(COLUMN_UNAME, userDTO.getUserName());
        values.put(COLUMN_PASS, userDTO.getPassword());
        values.put(COLUMN_DEVICE_ID, userDTO.getDevice_id());
        db.insert(TABLE_NAME, null, values);
    }
    public void showTable(){
        db = this.getReadableDatabase();
        String query = "select * from "+TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        if(c.moveToFirst()){
            do{
                System.out.println(c.getString(0));
                System.out.println(c.getString(1));
                System.out.println(c.getString(2));
                System.out.println(c.getString(3));
                System.out.println("...........................");
            } while (c.moveToNext());
        }
        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    }

    public String getUserNameByDeviceId(String deviceId){
        db = this.getReadableDatabase();
        String[] columns ={"username"};
        Cursor c = db.query(TABLE_NAME, columns, "device_id=?", new String[] { deviceId }, null, null, null);
        // String query = "select username from "+TABLE_NAME+ " where device_id = "+userDTO.getDevice_id();
        //  Cursor c = db.rawQuery(query, null);
        if(c.moveToFirst()){
            return c.getString(0);
        }
        return null;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }

    public boolean isValidLogin(String uName, String pass){
        System.out.println("uName:"+uName);
        db = this.getReadableDatabase();
        String query = "select username, password from "+TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        String a,b;
        b = "Not Found";
        if(c.moveToFirst()){
            do{
                a = c.getString(0);
                System.out.println("a:"+a);
                if(a.equals(uName)) {
                    b = c.getString(1);
                    System.out.println("b:"+b);
                    break;
                }
            } while (c.moveToNext());
        }

        if(b.equals(pass))
            return true;
        else
            return false;
    }
}
