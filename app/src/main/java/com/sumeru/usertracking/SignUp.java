package com.sumeru.usertracking;

import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.provider.Settings.Secure;

import com.sumeru.usertracking.dto.UserDTO;

/**
 * Created by Nihar on 8/10/2017.
 */
public class SignUp extends AppCompatActivity {

    private FingerprintManager fingerprintManager;
    DataBaseHelper helper = new DataBaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
    }

    public void onSignUp(View v){
        if(v.getId() == R.id.BSignUp){
            EditText uName = (EditText) findViewById(R.id.TFuname);
            EditText passFirst = (EditText) findViewById(R.id.TFpassFirst);
            EditText passSecond = (EditText) findViewById(R.id.TFpassSecond);

            String userName = uName.getText().toString();
            String pass1 = passFirst.getText().toString();
            String pass2 = passSecond.getText().toString();

            if(isEmpty(userName)) {
                Toast.makeText(SignUp.this, "User Name can't be empty!", Toast.LENGTH_SHORT).show();
            } else if(isEmpty(pass1) || isEmpty(pass2)) {
                Toast.makeText(SignUp.this, "Password can't be empty!", Toast.LENGTH_SHORT).show();
            } else if(!pass1.equals(pass2)) {
                Toast.makeText(SignUp.this, "Password don't match!!", Toast.LENGTH_SHORT).show();
            } else{

                UserDTO userDTO = new UserDTO();
                String ANDROID_ID = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
                System.out.println("ANDROID_ID^^^^^^^^^: "+ANDROID_ID);

                if(isFingerprintDetected())
                    userDTO.setDevice_id(ANDROID_ID); //set ID if fingerprint hardware present
                else
                    userDTO.setDevice_id(" ");
                userDTO.setUserName(userName);
                userDTO.setPassword(pass1);
                if(helper.isDuplicateUserName(userDTO)){
                    Toast.makeText(SignUp.this, "Username already taken!", Toast.LENGTH_SHORT).show();
                } else if((!userDTO.getDevice_id().equals(" ")) && helper.isDeviceRegistered(userDTO)){
                    Toast.makeText(SignUp.this, "User already registered from this device", Toast.LENGTH_SHORT).show();
                } else {
                    helper.insertUser(userDTO);
                    helper.showTable();

                    Toast.makeText(SignUp.this, "Sign Up Successful!!",Toast.LENGTH_LONG).show();

                    Intent i = new Intent(SignUp.this, MainActivity.class);
                    startActivity(i);
                }
            }
        }
    }
    private boolean isEmpty(String etText) {
        if (etText.trim().length() > 0)
            return false;

        return true;
    }

    private boolean isFingerprintDetected() {

        System.out.println("Build.VERSION.SDK_INT "+ Build.VERSION.SDK_INT+ ", "+Build.VERSION_CODES.M);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            if (fingerprintManager.isHardwareDetected()){
                System.out.println("^^^^^^^^^^^^^^^^^^^^^^^Hardware detected");
                return true;
            }
            return false;
        }
        return false;
    }
}
