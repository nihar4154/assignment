package com.sumeru.usertracking;

import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sumeru.usertracking.fingerprint.FingerPrint;

public class MainActivity extends AppCompatActivity {

    private FingerprintManager fingerprintManager;
    DataBaseHelper helper = new DataBaseHelper(this);
    DummyDBHelper dummyHelper = new DummyDBHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Insert Dummy Data
        dummyHelper.insertDummyData();

        if(isFingerprintDetected()){
            System.out.println("^^^^^^^^^^^^^^^^Fingerprint detected....Login");
            Button myButton = new Button(this);
            myButton.setText("Fingerprint Login");
            myButton.setAllCaps(false);
            myButton.setOnClickListener(getOnClickDoSomething(myButton));
            LinearLayout ll = (LinearLayout)findViewById(R.id.activity_main);
            ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            ll.addView(myButton, lp);
        }
    }

    View.OnClickListener getOnClickDoSomething(final Button button)  {
        return new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, FingerPrint.class);
                startActivity(i);
            }
        };
    }

    public void onLogin(View v) {
        if (v.getId() == R.id.LoginBtn) {
            EditText uName = (EditText) findViewById(R.id.TFuname);
            EditText pass =  (EditText) findViewById(R.id.TFpwd);
            String userName = uName.getText().toString();
            String password = pass.getText().toString();
            System.out.println("^^^^^^^^^^^^^^^^"+userName+ " "+password);
            if(helper.isValidLogin(userName, password)){

                if(isFingerprintDetected()){
                    //authenticate
                    Intent i = new Intent(MainActivity.this, FingerPrint.class);
                    i.putExtra("username",userName);
                    startActivity(i);
                } else {
                    Intent i = new Intent(MainActivity.this, MainDisplay.class);
                    //Intent i = new Intent(MainActivity.this, MainDisplay.class);
                    i.putExtra("username",userName);
                    startActivity(i);
                }
            } else{
                Toast.makeText(MainActivity.this, "Invalid Login!!", Toast.LENGTH_LONG).show();
            }


        }
    }
    private boolean isFingerprintDetected() {

        System.out.println("Build.VERSION.SDK_INT "+Build.VERSION.SDK_INT+ ", "+Build.VERSION_CODES.M);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            if (fingerprintManager.isHardwareDetected()){
                System.out.println("Hardware detected");
                return true;
            }
            return false;
        }
        return false;
    }


    public void onSignUp(View v){
        if(v.getId() == R.id.SignUpBtn){
            Intent i = new Intent(MainActivity.this, SignUp.class);
            startActivity(i);

        }
    }
}
