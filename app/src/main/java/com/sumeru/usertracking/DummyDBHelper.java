package com.sumeru.usertracking;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.sumeru.usertracking.dto.EmpDTO;
import com.sumeru.usertracking.dto.EmployeDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nihar on 8/20/2017.
 */
public class DummyDBHelper extends SQLiteOpenHelper {

    SQLiteDatabase db;
    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME ="users.db";
    private static final String TABLE_NAME ="users";
    private static final String COLUMN_ID ="id";
    private static final String COLUMN_NAME ="name";
    private static final String COLUMN_ADDRESS ="address";

    private static final String TABLE_CREATE ="CREATE TABLE users(id int primary key not null, "+
            " name text not null, address text not null)" ;

    public DummyDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("OnCreate method called......................");
        db.execSQL(TABLE_CREATE);
        this.db =db;
      //  insertDummyData();

    }

    public void insertDummyData(){

        db = this.getWritableDatabase();
        try {
            db.execSQL("insert into users (id,name,address) values(1,'Alexandra','Central Silk Board, Bommanahalli  Bengaluru  Karnataka');");
            db.execSQL("insert into users (id,name,address) values(2,'Talisha','Hebbal Flyover, Hebbal Kempapura, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(3,'Jone','Kempegowda International Airport, KIAL Road, Devanahalli, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(4,'Tomas','The Leela Palace Bengaluru, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(5,'Hellen','Park Plaza, Outer Ring Road, Marathahalli, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(6,'Rickey','Absolute Barbecue Marthahalli, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(7,'Joe','Phoenix Marketcity, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(8,'Dyan','Decathlon, Sarjapur Road, Chikkakannalli, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(9,'Krish','M Chinnaswamy Stadium, Bengaluru, Karnataka');");
            db.execSQL("insert into users (id,name,address) values(10,'Necole','Garuda Mall, Magrath Road, Ashok Nagar, Bengaluru, Karnataka');");
        }catch (SQLiteException se){
            System.out.println("Exception caught^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        }
/*
        String query = "select * from "+TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        //   int count = c.getCount();
        if(c.moveToFirst()){
            do{
                System.out.println(c.getString(0));
                System.out.println(c.getString(1));
                System.out.println(c.getString(2));
                System.out.println("...........................");
            } while (c.moveToNext());
        }
        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");*/

    }

    public EmployeDetails fetchUsersFromDB(){
        db = this.getReadableDatabase();
        String query = "select id,name,address from "+TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        EmployeDetails employeDetails = null;
        List<EmpDTO> list = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                EmpDTO empDTO = new EmpDTO();
                empDTO.setId(c.getInt(0));
                empDTO.setName(c.getString(1));
                empDTO.setAddress(c.getString(2));
                list.add(empDTO);
            } while (c.moveToNext());
        }
        employeDetails = new EmployeDetails();
        employeDetails.setEmployeList(list);
        return employeDetails;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }
}
